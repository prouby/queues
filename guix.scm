(use-modules ((guix licenses) #:prefix l:)
             (guix packages)
             (guix download)
             (guix gexp)
             (guix utils)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages pkg-config)
             (gnu packages guile))

(package
  (name "queues")
  (version "0.0.1")
  (source (local-file "." "queues" #:recursive? #t))
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf"   ,autoconf)
     ("automake"   ,automake)
     ("libtool"    ,libtool)
     ("pkg-config" ,pkg-config)))
  (inputs
   `(("guile" ,guile-2.2)))
  (synopsis "Functional queues implementation")
  (description
   "Functional queues implementation.")
  (home-page "https://framagit.org/prouby/queues")
  (license l:gpl3+))
