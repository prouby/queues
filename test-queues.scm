(use-modules (queues))

(display "*** Test of full fonctional queue implementation ***") (newline)

(define q  (list->queue   '(0 1 2 3 4 5 6 7 8 9)))
(define rq (list->queue-r '(0 1 2 3 4 5 6 7 8 9)))

;;; Display queue
(newline)
(display "(list->queue '(0 .. 9))\n  > ")
(display q) (newline)

(display "(list->queue-r '(0 .. 9))\n  > ")
(display rq) (newline)

;;; empty-queue?
(newline)
(display "(empty-queue? (make-queue))\n  > ")
(display (empty-queue? (make-queue))) (newline)
(display "(empty-queue? q)\n  > ")
(display (empty-queue? q)) (newline)

;;; Enq
(newline)
(display "(enq (deq q) -1)\n  > ")
(display (enq (deq q) -1)) (newline)

;;; Deql
(newline)
(display "(deql q)\n  > ")
(display (deql q)) (newline)
(display "(deql rq)\n  > ")
(display (deql rq)) (newline)

;;; Map on q
(newline)
(display "(mapq λ (enq (enq q 10) 11))\n  > ")
(mapq (λ (x)
        (display x)
        (display " ")
        x)
      (enq (enq q 10) 11)) (newline)

(display "(mapq λ (enq (enq rq -1) -2))\n  > ")
(mapq (λ (x)
        (display x)
        (display " ")
        x)
      (enq (enq rq -1) -2)) (newline)

(display "(define mq (mapq '+1 q))\n  > ")
(define mq (mapq (λ (x) (+ x 1)) q))
(display mq) (newline)

(display "(mapq λ mq)\n  > ")
(mapq (λ (x)
        (display x)
        (display " ")
        x)
      mq) (newline)

;;; Queue to list
(newline)
(display "(queue->list q)\n  > ")
(display (queue->list q)) (newline)
(display "(queue->r-list q)\n  > ")
(display (queue->r-list q)) (newline)

;;; Reverse queue
(newline)
(display "(reverse-queue q)\n  > ")
(display (reverse-queue q)) (newline)
(display "(reverse-queue (enq (deq rq) 10))\n  > ")
(display (reverse-queue (enq (deq q) 10))) (newline)

;;; Equal?
(newline)
(display "(queue-equal? q1 q1)\n  > ")
(display (queue-equal? (list->queue '(1 2 3 4))
                       (list->queue '(1 2 3 4)))) (newline)

(display "(queue-equal? q1 q2)\n  > ")
(display (queue-equal? (list->queue '(1 2 3 4))
                       (list->queue '(1 2 3 4 5)))) (newline)

(display "(queue-equal? q2 q1)\n  > ")
(display (queue-equal? (list->queue '(1 2 3 4 5))
                       (list->queue '(1 2 3 4)))) (newline)

(display "(queue-equal? q1 q1bis)\n  > ")
(display (queue-equal? (list->queue '(1 2 3 4))
                       (enq (deq (list->queue '(0 1 2 3))) 4))) (newline)

(display "*** End of test ***") (newline)
